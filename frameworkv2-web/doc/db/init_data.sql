INSERT INTO `power_group` VALUES (1,0,'默认组织机构','',0,'2016-05-29 13:21:37',NULL,1,1,'','320111');

INSERT INTO `power_menu` VALUES (1,0,'系统管理','admin','/admin','fa-cogs',6),
(2,1,'管理机构','group','/power/group/main.html','fa-users',1),
(3,1,'用户','user','/power/user/main.html','fa-user',2),
(4,1,'角色','role','/power/role/main.html','fa-odnoklassniki',3),
(5,1,'权限','permission','/power/permission/main.html','fa-lock',4),
(6,5,'通用','premission','/power/permission/main.html?type=0','fa-bars',1),
(7,5,'菜单','menu','/power/menu/main.html?type=1','fa-list-ul',2),
(8,1,'任务计划','powerschedule','/power/schedule/main','fa-clock-o',5),
(9,1,'系统监控','sysmonitor','/druid/','fa-area-chart',6),
(10,1,'数据字典','dictionary','/dictionary','fa-th-list',7),
(11,10,'字典分类','dicttype','/dict/info/main','fa-list-ol',1),
(12,10,'字典详情','dictdetail','/dictdetail/info/main','fa-list',2),
(13,1,'数据库维护','dbmanager','/power/dbmanager/main.do','fa-database',8);

INSERT INTO `power_permission` VALUES (1,1,'系统管理',1,'admin:manager',0,'2016-03-11 11:31:59','2016-05-23 15:30:44',6),
(2,2,'管理机构',1,'group:manager',0,'2016-03-11 11:36:00','2016-05-28 12:00:43',1),
(3,3,'用户',1,'user:manager',0,'2016-03-11 11:36:39',NULL,2),
(4,4,'角色',1,'role:manager',0,'2016-03-11 11:38:20',NULL,3),
(5,5,'权限',1,'permission:manager',0,'2016-03-11 11:40:03',NULL,4),
(6,6,'通用',1,'permission:normal',0,'2016-03-11 15:00:35',NULL,1),
(7,7,'菜单',1,'menu:manage',0,'2016-03-11 15:01:18',NULL,2),
(8,8,'任务计划',1,'powerschedule:manage',0,'2016-05-13 09:38:24',NULL,5),
(9,9,'系统监控',1,'sysmonitor:manage',0,'2016-05-25 22:02:49',NULL,6),
(10,10,'数据字典',1,'dictionary:manage',0,'2016-05-27 17:05:28',NULL,7),
(11,11,'字典分类',1,'dicttype:manage',0,'2016-05-27 17:06:30','2016-05-27 17:08:13',1),
(12,12,'字典详情',1,'dictdetail:manage',0,'2016-05-27 17:07:22','2016-05-27 18:21:25',2),
(13,13,'数据库维护',1,'dbmanager:manage',0,'2017-03-03 22:10:51','2017-03-22 14:41:08',8);

INSERT INTO `power_role` VALUES (1,'系统管理员','系统管理员角色',0,1,'2016-03-08 12:03:32','2016-05-13 09:40:37'),
(2,'普通管理员','普通管理员',0,1,'2016-05-29 20:24:30',NULL);

INSERT INTO `power_user` VALUES (1,'admin','orsP6i0RrSjRMO8cDcpaFQ==','0a9a8ca774ac2b4e2a037559bcbb40ab','系统管理员','系统管理员',2,18,'2016-03-08','13888888888','test@mail.com',0,0,1,NULL,NULL,NULL,'2016-03-08 10:43:37','2017-03-13 12:33:11'),;

INSERT INTO `rel_group_user` VALUES (0,6),
(1,1),
(1,4);

INSERT INTO `rel_role_permission` VALUES (1,1),
(1,2),
(1,3),
(1,4),
(1,5),
(1,6),
(1,7),
(1,8),
(1,9),
(1,10),
(1,11),
(1,12),
(1,14),
(1,15),
(1,16),
(1,17),
(1,18),
(1,19),
(1,20),
(1,21),
(1,22),
(1,23);

INSERT INTO `rel_user_role` VALUES (1,1),
(1,2);

INSERT INTO `t_dict` VALUES (1,'group_type','组织机构分类','组织机构分类','2016-06-07 11:34:17',NULL,0);

INSERT INTO `t_dict_detail` VALUES (1,1,'区县','1','4','1','1','1','1','2016-06-07 11:35:42','2017-03-03 23:10:40',0),
(2,1,'街道','2','2','','1',NULL,'','2017-03-03 23:08:55','2017-03-03 23:10:44',0),
(3,1,'社区','3','3','','1',NULL,'','2017-03-03 23:09:08','2017-03-03 23:10:48',0);