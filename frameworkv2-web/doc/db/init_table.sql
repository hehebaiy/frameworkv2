DROP TABLE IF EXISTS `power_group`;
CREATE TABLE `power_group` (
    `group_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '组织机构编号',
    `group_pid` bigint(20) NOT NULL DEFAULT '0' COMMENT '上级组织机构编号',
    `group_name` varchar(100) NOT NULL COMMENT '组织机构名称',
    `group_desc` varchar(255) DEFAULT NULL COMMENT '组织机构描述',
    `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标志\n            0未删除\n            1已删除',
    `create_date` datetime NOT NULL COMMENT '创建时间',
    `modify_date` datetime DEFAULT NULL COMMENT '更新时间',
    `order_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '排序号',
    `group_type` tinyint(1) DEFAULT NULL COMMENT '机构类型（1总队2大队3中队4科室）',
    `group_level` varchar(255) DEFAULT '' COMMENT '组织机构层级关系',
    `group_code` varchar(100) DEFAULT NULL,
    PRIMARY KEY (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8mb4;



DROP TABLE IF EXISTS `power_menu`;
CREATE TABLE `power_menu` (
    `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单编号',
    `menu_pid` bigint(20) NOT NULL DEFAULT '0' COMMENT '上级菜单编号',
    `menu_name` varchar(255) NOT NULL COMMENT '菜单名称',
    `menu_code` varchar(255) DEFAULT NULL COMMENT '菜单编码',
    `menu_url` varchar(255) DEFAULT NULL COMMENT '菜单地址',
    `menu_icon` varchar(255) DEFAULT NULL COMMENT '菜单图标',
    `order_id` bigint(20) DEFAULT '0',
    PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4;



DROP TABLE IF EXISTS `power_permission`;
CREATE TABLE `power_permission` (
    `permission_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '权限编号',
    `privilege_id` bigint(20) DEFAULT NULL COMMENT '受控对象编号',
    `permission_name` varchar(100) NOT NULL COMMENT '权限名称',
    `permission_type` tinyint(4) DEFAULT NULL COMMENT '权限类型\n            0普通权限\n            1菜单权限\n            2操作权限',
    `permission_code` varchar(100) NOT NULL COMMENT '权限编码',
    `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标志',
    `create_date` datetime NOT NULL COMMENT '创建时间',
    `modify_date` datetime DEFAULT NULL COMMENT '更新时间',
    `order_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '排序号',
    PRIMARY KEY (`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4;



DROP TABLE IF EXISTS `power_role`;
CREATE TABLE `power_role` (
    `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色编号',
    `role_name` varchar(100) NOT NULL COMMENT '角色名称',
    `role_desc` varchar(255) DEFAULT NULL COMMENT '角色描述',
    `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标志',
    `enabled` tinyint(4) NOT NULL DEFAULT '1' COMMENT '启用标志',
    `create_date` datetime NOT NULL COMMENT '创建时间',
    `modify_date` datetime DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;



DROP TABLE IF EXISTS `power_schedule`;
CREATE TABLE `power_schedule` (
    `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '任务编号',
    `job_name` varchar(255) DEFAULT NULL COMMENT '任务名称',
    `job_group` varchar(255) DEFAULT NULL COMMENT '任务分组',
    `job_cron` varchar(20) DEFAULT NULL COMMENT '任务执行时间表达式',
    `job_class_path` varchar(255) DEFAULT NULL COMMENT '任务执行类完整路径',
    `job_status` varchar(45) DEFAULT NULL COMMENT '任务状态 stop 停止 start 启动 pause 暂停',
    `job_description` varchar(255) DEFAULT NULL COMMENT '任务描述',
    `create_date` datetime DEFAULT NULL COMMENT '创建时间',
    `modify_date` datetime DEFAULT NULL COMMENT '修改时间',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `power_user`;
CREATE TABLE `power_user` (
    `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户编号',
    `user_account` varchar(255) NOT NULL COMMENT '用户账号',
    `user_password` varchar(255) NOT NULL COMMENT '用户登录密码',
    `user_salt` varchar(32) NOT NULL COMMENT '用户盐',
    `user_nickname` varchar(100) DEFAULT NULL COMMENT '用户昵称',
    `user_realname` varchar(100) DEFAULT NULL COMMENT '用户姓名',
    `user_sex` tinyint(4) DEFAULT NULL COMMENT '用户性别',
    `user_age` tinyint(3) DEFAULT NULL COMMENT '用户年龄',
    `user_birthday` date DEFAULT NULL COMMENT '出生日期',
    `user_phone` varchar(11) DEFAULT NULL COMMENT '用户手机',
    `user_email` varchar(255) NOT NULL COMMENT '用户邮箱',
    `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标志',
    `locked` tinyint(4) NOT NULL DEFAULT '0' COMMENT '锁定标志',
    `enabled` tinyint(4) NOT NULL DEFAULT '0' COMMENT '启用标志',
    `last_login_date` datetime DEFAULT NULL COMMENT '最后登录时间',
    `failed_login_date` datetime DEFAULT NULL COMMENT '登录失败时间',
    `failed_count` tinyint(4) DEFAULT '0' COMMENT '登录失败次数',
    `create_date` datetime NOT NULL COMMENT '创建时间',
    `modify_date` datetime DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8mb4;



DROP TABLE IF EXISTS `rel_group_user`;
CREATE TABLE `rel_group_user` (
    `group_id` bigint(20) NOT NULL COMMENT '组织机构编号',
    `user_id` bigint(20) NOT NULL COMMENT '用户编号',
    PRIMARY KEY (`group_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



DROP TABLE IF EXISTS `rel_role_permission`;
CREATE TABLE `rel_role_permission` (
    `role_id` bigint(20) NOT NULL COMMENT '角色编号',
    `permission_id` bigint(20) NOT NULL COMMENT '权限编号',
    PRIMARY KEY (`role_id`,`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



DROP TABLE IF EXISTS `rel_user_role`;
CREATE TABLE `rel_user_role` (
    `user_id` bigint(20) NOT NULL COMMENT '用户编号',
    `role_id` bigint(20) NOT NULL COMMENT '角色编号',
    PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



DROP TABLE IF EXISTS `t_dict`;
CREATE TABLE `t_dict` (
    `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
    `type_code` varchar(255) DEFAULT NULL COMMENT '类型编码',
    `name` varchar(255) DEFAULT NULL COMMENT '名称',
    `remark` varchar(255) DEFAULT NULL COMMENT '备注',
    `create_date` datetime DEFAULT NULL COMMENT '创建时间',
    `modify_date` datetime DEFAULT NULL COMMENT '修改时间',
    `deleted` tinyint(1) DEFAULT '0' COMMENT '删除标志',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4;



DROP TABLE IF EXISTS `t_dict_detail`;
CREATE TABLE `t_dict_detail` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `dict_type` int(11) DEFAULT NULL COMMENT '所属分类',
    `detail_name` varchar(255) DEFAULT NULL COMMENT '名称',
    `detail_code` varchar(255) DEFAULT NULL COMMENT '编码',
    `detail_sort` varchar(45) DEFAULT NULL COMMENT '排序号',
    `detail_type` varchar(255) DEFAULT NULL COMMENT '类型',
    `detail_state` varchar(255) DEFAULT NULL COMMENT '状态',
    `detail_content` varchar(255) DEFAULT NULL COMMENT '内容',
    `detail_remark` varchar(255) DEFAULT NULL COMMENT '备注',
    `create_date` datetime DEFAULT NULL COMMENT '创建时间',
    `modify_date` datetime DEFAULT NULL COMMENT '修改时间',
    `deleted` tinyint(1) DEFAULT '0' COMMENT '删除标志',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=136 DEFAULT CHARSET=utf8mb4;