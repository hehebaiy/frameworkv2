package com.codecrane.core.power.service.impl;

import com.codecrane.core.power.entity.PowerUser;
import com.codecrane.core.power.service.PowerUserService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * PowerUserServiceImpl Tester.
 *
 * @author <Authors name>
 * @version 1.0
 * @since <pre>六月 30, 2016</pre>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration(value = "src/main/webapp")
@ContextConfiguration(name = "parent", locations = {"classpath*:spring/app-config.xml", "classpath*:spring/shiro-config.xml", "classpath*:spring/mvc-config.xml"})
public class PowerUserServiceImplTest {

    @Autowired
    private PowerUserService powerUserService;

    @Before
    public void before() throws Exception {
    }

    @After
    public void after() throws Exception {
    }

    /**
     * Method: save(PowerUser powerUser)
     */
    @Test
    public void testSave() throws Exception {
//TODO: Test goes here... 
    }

    /**
     * Method: saveBatch(List<PowerUser> powerUsers)
     */
    @Test
    public void testSaveBatch() throws Exception {
//TODO: Test goes here... 
    }

    /**
     * Method: modify(PowerUser powerUser)
     */
    @Test
    public void testModify() throws Exception {
//TODO: Test goes here... 
    }

    /**
     * Method: delete(long powerUserId)
     */
    @Test
    public void testDelete() throws Exception {
//TODO: Test goes here... 
    }

    /**
     * Method: deleteByIds(long[] powerUserIds)
     */
    @Test
    public void testDeleteByIds() throws Exception {
//TODO: Test goes here... 
    }

    /**
     * Method: findById(long powerUserId)
     */
    @Test
    public void testFindById() throws Exception {
        System.out.println(powerUserService.findById(2));
        System.out.println(powerUserService.findById(2));
        System.out.println(powerUserService.findById(2));
        System.out.println(powerUserService.findById(2));
    }

    /**
     * Method: findAllPowerUser()
     */
    @Test
    public void testFindAllPowerUser() throws Exception {
//TODO: Test goes here... 
    }

    /**
     * Method: doLoginAuth(String userAccount, String userPassword, String vcode)
     */
    @Test
    public void testDoLoginAuth() throws Exception {
//TODO: Test goes here... 
    }

    /**
     * Method: findByUserAccount(String userAccount)
     */
    @Test
    public void testFindByUserAccount() throws Exception {

        System.out.println(powerUserService.findByUserAccount("xingning"));

        Thread.sleep(2000);
        System.out.println(powerUserService.findByUserAccount("xingning"));

        Thread.sleep(2000);
        System.out.println(powerUserService.findByUserAccount("xingning"));

    }

    /**
     * Method: findUsersByGroupId(long groupId)
     */
    @Test
    public void testFindUsersByGroupId() throws Exception {
//TODO: Test goes here... 
    }

    /**
     * Method: finUsersByRoleId(long roleId)
     */
    @Test
    public void testFinUsersByRoleId() throws Exception {
//TODO: Test goes here... 
    }

    /**
     * Method: findUsersByPermissionId(long permissionId)
     */
    @Test
    public void testFindUsersByPermissionId() throws Exception {
//TODO: Test goes here... 
    }

    /**
     * Method: findUsersByMenuId(long menuId)
     */
    @Test
    public void testFindUsersByMenuId() throws Exception {
//TODO: Test goes here... 
    }

    /**
     * Method: doLogout(long userId)
     */
    @Test
    public void testDoLogout() throws Exception {
//TODO: Test goes here... 
    }

    /**
     * Method: changePassword(String account, String oldPassword, String newPassword)
     */
    @Test
    public void testChangePassword() throws Exception {
//TODO: Test goes here... 
    }

    /**
     * Method: getCurrentUserFromShiro()
     */
    @Test
    public void testGetCurrentUserFromShiro() throws Exception {
//TODO: Test goes here... 
    }


} 
