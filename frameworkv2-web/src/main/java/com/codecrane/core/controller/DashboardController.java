package com.codecrane.core.controller;

import com.codecrane.core.power.entity.PowerGroup;
import com.codecrane.core.power.entity.PowerUser;
import com.codecrane.core.power.service.PowerGroupService;
import com.codecrane.core.power.service.PowerUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 仪表盘主页控制器
 * Created by crane on 16/8/15.
 */
@Controller
@RequestMapping("/dashboard")
public class DashboardController {


    @Autowired
    private PowerUserService powerUserService;

    @Autowired
    private PowerGroupService powerGroupService;

    /**
     * 仪表盘主页面
     *
     * @param model
     * @return
     */
    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    public String dashboardIndexPage(ModelMap model) {

        // 查询当前用户可选管理机构
        PowerUser user = powerUserService.getCurrentUserFromShiro();
        if (null != user) {
            model.addAttribute("groupId", user.getGroupId());
            PowerGroup group = powerGroupService.findById(user.getGroupId());
            model.addAttribute("groupType", group.getGroupType());
        }

        return "/power/dashboard";
    }


}
