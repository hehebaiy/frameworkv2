package com.codecrane.core.power.service.impl;

import com.codecrane.core.power.entity.DictDetail;
import com.codecrane.core.power.mapper.DictDetailMapper;
import com.codecrane.core.power.service.DictDetailService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


/**
 * 数据字典详情信息 服务
 *
 * @author Crane(hehebaiy@gmail.com)
 * @version 0.1
 *          <br/>
 *          <br/>
 */
@Service
public class DictDetailServiceImpl implements DictDetailService {

    @Autowired
    private DictDetailMapper dictDetailMapper;

    @Override
    @CacheEvict(value = "dictDataCache",allEntries = true)
    public int save(DictDetail dictDetail) {
        int result = 0;
        if (null != dictDetail) {
            result = dictDetailMapper.insert(dictDetail);
        }
        return result;
    }

    @Override
    @CacheEvict(value = "dictDataCache",allEntries = true)
    public int saveBatch(List<DictDetail> dictDetails) {
        int result = 0;
        if (null != dictDetails && dictDetails.size() > 0) {
            result = dictDetailMapper.insertBatch(dictDetails);
        }
        return result;
    }

    @Override
    @CacheEvict(value = "dictDataCache",allEntries = true)
    public int modify(DictDetail dictDetail) {
        int result = 0;
        if (null != dictDetail) {
            dictDetail.setModifyDate(new Date());
            result = dictDetailMapper.update(dictDetail);
        }
        return result;
    }

    @Override
    @CacheEvict(value = "dictDataCache",allEntries = true)
    public int delete(Integer dictDetailId) {
        int result = 0;
        if (dictDetailId > 0) {
            result = dictDetailMapper.delete(dictDetailId);
        }
        return result;
    }

    @Override
    @CacheEvict(value = "dictDataCache",allEntries = true)
    public int deleteByIds(Integer[] dictDetailIds) {
        int result = 0;
        if (dictDetailIds.length > 0) {
            result = dictDetailMapper.deleteByIds(dictDetailIds);
        }
        return result;
    }

    @Override
    @Cacheable(value = "dictDataCache",key = "'findById_'+#dictDetailId")
    public DictDetail findById(Integer dictDetailId) {
        return dictDetailMapper.queryByDictDetailId(dictDetailId);
    }

    @Override
    public List<DictDetail> findAllDictDetail() {
        return dictDetailMapper.queryAllDictDetail();
    }

    @Override
    public List<DictDetail> findByCnd(DictDetail dictDetail) {
        if (null != dictDetail) {
            return dictDetailMapper.queryByCnd(dictDetail);
        }
        return null;
    }

    @Override
    @Cacheable(value = "dictDataCache",key = "'findByTypeCode_|'+#typeCode+'|-|'+#detailType")
    public List<DictDetail> findByTypeCode(String typeCode, String detailType) {
        if (StringUtils.isNoneBlank(typeCode)) {
            return dictDetailMapper.queryByTypeCode(typeCode, detailType);
        }
        return null;
    }
}