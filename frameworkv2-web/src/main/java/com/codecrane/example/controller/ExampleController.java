package com.codecrane.example.controller;

import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 示例Controller
 * Created by crane on 16/6/28.
 */
@RequestMapping("/example")
public class ExampleController {

    //TODO 文件上传示例

    //TODO 文件下载示例

    //TODO Excel导入示例

    //TODO Excel导出示例

    //TODO bootstrap-table组件示例

    //TODO ztree组件示例

    //TODO 编辑器组件示例

    //TODO layer弹窗组件示例

    //TODO ladate日期组件示例

    //TODO laytpl模板组件示例

    //TODO fullcalender日历组件示例

    //TODO

}
