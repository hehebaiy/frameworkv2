/**
 * 地图相关工具
 * Created by crane on 16/9/14.
 */

/**
 * 经纬度转度分秒
 * @param latLngVal
 * @constructor
 */
function ChangeToDFM(latLngVal) {
    var str1 = latLngVal.split(".");
    var du = str1[0];
    var tp = "0." + str1[1]
    var tp = String(tp * 60);		//这里进行了强制类型转换
    var str2 = tp.split(".");
    var fen = str2[0];
    tp = "0." + str2[1];
    tp = tp * 60;
    var miao = tp;
    return du + "°" + fen + "'" + miao + "\"";
}

/**
 * 度分秒转经纬度
 * @param dfm 用逗号隔开
 * @constructor
 */
function ChangeToDu(dfm) {
    var dfmVal = dfm.split(',');
    var d = dfmVal[0];
    var f = dfmVal[1];
    var m = dfmVal[2];

    var f = parseFloat(f) + parseFloat(m / 60);
    var du = parseFloat(f / 60) + parseFloat(d);
    return du;
}

