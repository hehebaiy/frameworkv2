# 开发框架v2

 [TOC]

## 初始化框架步骤

### 1、数据库准备
  * 类型：MySQL 5.6+
  * 初始化表：执行frameworkv2-web中do目录下的`init_table.sql`和`init_data.sql`,注意顺序

## 2、数据库连接配置
    > 由于数据库密码经过加密，首先需要通过druid配置加密后的密码和公钥，配置方法如下
    
```
# 命令行下执行，druid-1.0.16.jar是你本地实际包的全路径，you_password是待加密的明文密码
java -cp druid-1.0.16.jar com.alibaba.druid.filter.config.ConfigTools you_password
```
  * 加密后将publickey和password拷贝到db.properties对应配置中
  
## 3、工程初始化
  * 一般情况下，你要先通过maven在顶层目录中执行一遍install，将config和core安装到本地的maven仓库
  * 执行web中的jetty:run命令可以直接运行项目
  * eclipse中需要通过tomcat启动的根据需要将web加入到tomcat的server中就可以
  
> 按照以上步骤基本可以完成系统的初始化，系统默认账号是：admin，密码：123456